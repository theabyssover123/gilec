#ifndef __MY_STRING_H
#define __MY_STRING_H

//!#include <va_list.h>
#include <string>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <algorithm>

#ifdef QT_GUI_LIB
  #include <QString>
#endif

#include "mytypes.h"

typedef char            TCHAR;
typedef const char *    LPCTSTR;
typedef const char *    LPCSTR;
typedef       char *    LPSTR;
typedef unsigned int    UINT;
typedef unsigned char   UCHAR;

typedef std::string     String;
typedef std::string     Tstring;

//-------------------------------------------------------------------------------
//������� ��������� ����� ������� ��� std::string
template<class Tstr >
inline
//!NOTE ������ ��������� const string&, ��� ��� ��� ��������� �������� ��������� ��������
const char * to_ascii(/* const  */Tstr &sin)
{
  return sin.c_str();
}

#ifdef QT_GUI_LIB
//-------------------------------------------------------------------------------
//�������������
template<>
inline
//!NOTE ������ ��������� const string&, ��� ��� ��� ��������� �������� ��������� ��������
const char * to_ascii(/* const  */QString &sin)
{
  return sin.toAscii().data();
}
#endif

//-------------------------------------------------------------------------------
//������� ��������� ����� ������� ��� std::string
template<class Tstr >
inline
Tstr & vprintf(Tstr &sin, LPCTSTR format, va_list ParList)
{
      //!TODO �������� ���� ����������
      char buf[4096]; //?����� ������� ����� ��������� ������
      vsprintf(&buf[0], format, ParList);
      sin = buf;
      return sin;
}
//-------------------------------------------------------------------------------
template<class Tstr >
inline
Tstr & sprintf(Tstr &sin, LPCTSTR format, ...)
{
      va_list paramList;
      va_start(paramList, format);
      vprintf(sin, format, paramList);  //     cat_vprintf(format, paramList);
      va_end(paramList);
      return sin;
}
//-------------------------------------------------------------------------------
template<class Tstr >
inline
Tstr & cat_sprintf(Tstr &sin, LPCTSTR format, ...)
{
      va_list paramList;
      va_start(paramList, format);
      std::string _s;
      vprintf(_s,format, paramList);  //     cat_vprintf(format, paramList);
      va_end(paramList);
      
      sin += _s;
      return sin;
}
#ifdef TRASH
//-------------------------------------------------------------------------------
inline 
void my_tolower (std::string::value_type i) 
{
  cout << " " << i;
}
#endif
//-------------------------------------------------------------------------------
inline
const String
str_tolower(const String &sin)
{
    String sout = sin;
    //std::for_each(sout.begin(), sout.end(), ::tolower);
    //second.resize(first.size());     // allocate space
    std::transform (sin.begin(), sin.end(), sout.begin(), ::tolower);    
    return sout;
}

//-------------------------------------------------------------------------------

#ifdef TRASH

class String : public std::string
{
    //String();
   public:
	// constructs empty std::string
	String(): std::string(){}
	// copy constructor
	String(const std::string& stringSrc): std::string(stringSrc){}
	// from a single character
	String(TCHAR ch, int nRepeat = 1):  std::string( ch, nRepeat){}
	// from an ANSI string (converts to TCHAR)
	String(LPCSTR lpsz):    std::string(lpsz){}
	// subset of characters from an ANSI string (converts to TCHAR)
	String(LPCSTR lpch, int nLength):   std::string(lpch, nLength){}
	// from unsigned characters
	//String(const unsigned char* psz):   std::string(psz){}
    #ifdef __UNICODE
    	// from a UNICODE string (converts to TCHAR)
    	String(LPCWSTR lpsz):   std::string(lpsz){}
    	// subset of characters from a UNICODE string (converts to TCHAR)
    	String(LPCWSTR lpch, int nLength):  std::string(lpch, nLength){}
    #endif
    virtual ~String(){};
    String&  sprintf(LPCTSTR format, ...){
          va_list paramList;
          va_start(paramList, format);
          FormatV(format, paramList);  //     cat_vprintf(format, paramList);
          va_end(paramList);
          return *this;
    }
    String&  cat_sprintf(LPCTSTR format, ...){
          va_list paramList;
          va_start(paramList, format);
          String s;
          s.FormatV(format, paramList);  //     cat_vprintf(format, paramList);
          va_end(paramList);
          *this += s;
          return *this;
    }
    int Length(){return size();}
    void FormatV(LPCTSTR format, va_list ParList){
        //!TODO �������� ���� ����������
        char buf[1024]; //����� ������� ����� ��������� ������
        vsprintf(&buf[0], format, ParList);
        *this = buf;
    }
  
};
#endif

#if defined( _MSC_VER )
  //��������� ��� MS
  class String : public CString
  {
     //String();
   public:
	// constructs empty CString
	String():CString(){}
	// copy constructor
	String(const CString& stringSrc):CString(stringSrc){}
	// from a single character
	String(TCHAR ch, int nRepeat = 1):CString( ch, nRepeat){}
	// from an ANSI string (converts to TCHAR)
	String(LPCSTR lpsz):CString(lpsz){}
	// from a UNICODE string (converts to TCHAR)
	String(LPCWSTR lpsz):CString(lpsz){}
	// subset of characters from an ANSI string (converts to TCHAR)
	String(LPCSTR lpch, int nLength):CString(lpch, nLength){}
	// subset of characters from a UNICODE string (converts to TCHAR)
	String(LPCWSTR lpch, int nLength):CString(lpch, nLength){}
	// from unsigned characters
	String(const unsigned char* psz):CString(psz){}
    virtual ~String(){};
    String& __cdecl     sprintf(LPCTSTR format, ...){
          va_list paramList;
          va_start(paramList, format);
          FormatV(format, paramList);  //     cat_vprintf(format, paramList);
          va_end(paramList);
          return *this;
    }
    String& __cdecl cat_sprintf(LPCTSTR format, ...){
          va_list paramList;
          va_start(paramList, format);
          String s;
          s.FormatV(format, paramList);  //     cat_vprintf(format, paramList);
          va_end(paramList);
          *this += s;
          return *this;
    }
    TCHAR* c_str() const {
         return GetData()->data();
    }     
    int Length(){return GetLength();}
     
  };
  

#endif

//------------------------------------------------------------------------------------------



#endif

