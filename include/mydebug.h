#ifndef __MY_DEBUG_H
#define __MY_DEBUG_H

#include <iostream> //for cout
#include <sys/types.h> //for getpid
#include <unistd.h> //for getpid

//#define __DEBUG_MSG
#ifdef __DEBUG_MSG
    #define __DMSG(STR)    std::cout << __FILE__ << ": " << __func__ << "(): [" <<__LINE__ << "] "<< STR << std::endl
#else
    #define __DMSG(STR)
#endif

#ifdef __PREF
  #define __MSG(STR)    std::cout << __PREF            << STR << std::endl
#else
  #define __MSG(STR)    std::cout << "------" << ": "  << STR << std::endl
#endif

#ifdef __PREF
  #define __XMSG(STR)    std::cout << std::hex << std::showbase << __PREF            << STR << std::dec << std::endl
#else
  #define __XMSG(STR)    std::cout << std::hex << std::showbase << "------" << ": "  << STR << std::dec << std::endl
#endif

//#ifndef __PREF
//    #define  __PREF  "------ "
//#endif    

//#define  __DEBUG

// #define __DBGINFO
// #ifdef __DBGINFO
    // DBGINFO(X...)   do{ X; ... } while (0)
// #else
    // DBGINFO(X...)
// #endif    


//#define _ERR_  /* � �� � ������ ����������� �������� */
//#undef  _ERR_

//#define _LOG_
//#undef  _LOG_

//#include "axis_error.h"

#include <stdarg.h>
#include <stdio.h>
#include <time.h>

#define  __TIME_TEMPL  "[%d%m%y_%Hh%Mm%Ss]"

inline
int my_printf(const char *pref, const char *format, ...)
{
   char tbuf[128]; //?����� ������� ����� ��������� ������
   char fbuf[1024]; //?����� ������� ����� ��������� ������
   int res =0;
   time_t ctime = time(NULL); 
   
   struct tm *  curr_time = localtime( &ctime );
   (void) strftime(&tbuf[0], sizeof(tbuf), __TIME_TEMPL , curr_time);
	
	va_list paramList;
	va_start(paramList, format);
	//int vsnprintf(char *str, size_t size, const char *format, va_list ap);
    vsnprintf(&fbuf[0], sizeof(fbuf), format, paramList);
	va_end(paramList);
	
	res = fprintf(stderr, "%s [%d]%s %s", pref, getpid(), tbuf, fbuf);
    //? fflush( stderr );
	return res;
}


#ifndef DBGERR
    #ifdef _ERR_
	  #define DBGERR(X,...) my_printf("---err:", "%s():" X "\n", __func__,  __VA_ARGS__)
	#else
	  #define DBGERR(...)
	#endif
#endif

#ifndef DBGLOG
	#ifdef _LOG_
	  #define DBGLOG(X,...) my_printf("---log:", "%s():" X "\n", __func__,  __VA_ARGS__)
	#else
	  #define DBGLOG(...)
	#endif
#endif

#include <ios>
#include "mystring.h"

template<typename Targ>
inline
void dump_buf(Targ * arr, size_t items, Size_t offset, const Tstring& fstr, size_t num_cols = 16)
{
    using namespace std;
    
    //cout.flags ( ios::right | ios::hex | ios::showbase );
    // cout.width ( sizeof(Targ) * 2);
    // cout.fill('0');
    std::cout /* << __PREF */ << fstr << ": items=" << std::hex << std::showbase << items << " by " << sizeof(arr[0]);
    Tstring sout;
    Tstring sform; sprintf(sform, " %%0%ux", sizeof( arr[0] ) *2 );
    
    for( size_t ___ii = 0; ___ii < items; ___ii++ )	{
        if( (___ii % num_cols) == 0) {
          //std::cout << std::endl << std::hex << std::showbase <<   << ":";
          cat_sprintf(sout, "\n0x%04lx:", ( offset + ___ii * sizeof( arr[0] ) ) );
        }
        //printk(" " __0X "%0"  NCHARS  "x", p_buf[i]);
        cat_sprintf(sout, sform.c_str(), arr[___ii]);
    }
    std::cout << sout << std::dec << std::endl;
}


#endif
