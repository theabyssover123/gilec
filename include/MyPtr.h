#ifndef __MY_PTR_H
#define __MY_PTR_H

#include <assert.h>

///////////////////////////////////////////////////////////
// TPtrBase
// Base class for automation pointer management.
// Provides basic implementation for  smart pointer object.
///////////////////////////////////////////////////////////

#ifndef _ASSERTE_
  #define _ASSERTE_(X)  assert(X)
#endif

// verify that types are complete for increased safety

template<class T> inline void checked_delete(T * x)
{
    // intentionally complex - simplification causes regressions
    typedef char type_must_be_complete[ sizeof(T)? 1: -1 ];
    (void) sizeof(type_must_be_complete);
    delete x;
}

template<class T>
class TPtrBase
{
public:
  // Methods implementing pointer semantics
  //
  T&  operator *  ()    const { _ASSERTE_(m_ptr); return *m_ptr; }
      operator T* ()    const { return m_ptr;}
      bool operator !() const { return m_ptr == 0; }
  T*   operator->()    const  { _ASSERTE_(m_ptr); return m_ptr;  }
  T**  operator &() /*const*/ { return &m_ptr; }

  // Comparison operators
  //
  bool operator == (T                 *rhs) const { return m_ptr == rhs;}
  bool operator == (const TPtrBase<T> &rhs) const { return m_ptr == rhs.m_ptr; }
  bool operator != (T                 *rhs) const { return m_ptr != rhs;}
  bool operator != (const TPtrBase<T> &rhs) const { return m_ptr != rhs.m_ptr; }
  //virtual ~TPtrBase(){}

protected:
   TPtrBase()     : m_ptr(0) {}
   TPtrBase(T* p) : m_ptr(p) {}

   T* get        () const   { return m_ptr;    }
   T* release    ()         { return reset(0); }
   T* reset      (T* p = 0) { T* tmp = m_ptr; m_ptr = p; return tmp; }
   
//public:
   // Actual data
   T* m_ptr;

private:
    // Prevent new/delete
    //!void* operator new(size_t);
    void  operator delete(void* /*p*/){/*::delete p;*/}; //? ������ �������� delete ��������� (����� ������ ���� ������ new TPtr<>)
};

//////////////////////////////////////////////////////////
// TPtr: Smart pointer object for non-array pointers
//////////////////////////////////////////////////////////

template<class T>
class TPtr : public TPtrBase<T>
{
    
public:
  TPtr()             : TPtrBase<T>()              {}
  TPtr(T*       src) : TPtrBase<T>(src)           {}
  TPtr(TPtr<T>& src) : TPtrBase<T>(src.release()) {}
 ~TPtr()                                          {clear(0);}

  // Assignment operators
  TPtr& operator = (TPtr<T>& src)
  {
    // Transfer ownership of pointer to receiver
    reset(src.release());
    return *this;
  }
  TPtr& operator = (T* src)
  {
    clear(src);
    return *this;
  }
  T*  operator& () const
  {
    return TPtrBase<T>::get();
  }

  // Clear object - free pointer
  void clear(T *src)
  {
    if (TPtrBase<T>::m_ptr)
      delete  TPtrBase<T>::m_ptr; //or use checked_delete() 
    TPtrBase<T>::m_ptr = src;
  }
};

//////////////////////////////////////////////////////////
// TAPtr: Smart pointer object for array new'ed memory
//////////////////////////////////////////////////////////

template<class T>
class TAPtr : public TPtrBase<T>
{
public:
  TAPtr()             : TPtrBase<T>()              {}
  TAPtr(T      src[]) : TPtrBase<T>(src)           {}
  TAPtr(TPtr<T>& src) : TPtrBase<T>(src.release()) {}
 ~TAPtr()                                          {clear(0);}

  // Assignment operators
  TAPtr& operator = (TAPtr<T>& src)
  {
    // Transfer ownership of pointer to receiver
    reset(src.release());
    return *this;
  }
  TAPtr& operator = (T src[])
  {
    clear(src);
    return *this;
  }

  // Subscript operator (for array)
  T& operator [](int i) const
  {
    _ASSERTE_(TPtrBase<T>::m_ptr);
    return TPtrBase<T>::m_ptr[i];
  }

  // Clear object - free pointer
  void clear(T src[])
  {
    if (TPtrBase<T>::m_ptr)
      delete [] TPtrBase<T>::m_ptr;
    TPtrBase<T>::m_ptr = src;
  }
};
#endif
