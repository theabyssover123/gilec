#ifndef __MY_LOG_CLASS_H
#define __MY_LOG_CLASS_H

#include <time.h>
#include <iostream>
#include <fstream>
#include <errno.h>

#include "mytypes.h"
#include "mystring.h"
#include "my_except.h"

#define  NAME_W_TIME_TEMPL  "%d%m%y_%Hh%Mm%Ss"

#define  __LOG_EXT  ".log"

#ifndef __LOG_PREFIX
#define __LOG_PREFIX  "  "
#endif

#define _DBG_STEPS        do { std::cout << "."; std::cout.flush(); } while(0)
#define _DBG_STEPS_STOP   do { std::cout << std::endl; std::cout.flush(); } while(0)


//------------------------------------------------------------------------------------------
inline
time_t Now()
{
    return time(NULL);
}

// char *asctime(const struct tm *tm);
// char *asctime_r(const struct tm *tm, char *buf);
// char *ctime_r(const time_t *timep, char *buf);
// char *ctime(const time_t *timep);
// struct tm *gmtime(const time_t *timep);
// struct tm *gmtime_r(const time_t *timep, struct tm *result);
// struct tm *localtime(const time_t *timep);
// struct tm *localtime_r(const time_t *timep, struct tm *result);
// time_t mktime(struct tm *tm);

//------------------------------------------------------------------------------------------
inline
const
String Time2Str(LPCSTR format, time_t time)
{
   char         sbuf[256];

   struct tm *  curr_time = localtime(& time);
   (void) strftime(sbuf, sizeof(sbuf), format , curr_time);

   return String(sbuf);
}
//------------------------------------------------------------------------------------------
inline
const
String GetTime(LPCSTR format, time_t delta = 0)
{
   return Time2Str(format, Now() + delta);
}

#define _LOGFILE_MAX_SIZE  300000 //bytes

//------------------------------------------------------------------------------------------
class TMyLog
{
  private:
    String         _fn;
    String         _orig_fn;
    std::ofstream  _fout; //(fname.c_str(), std::ios_base::in | std::ios_base::out | std::ios_base::app); //in  out  trunc
    String         _sout;
    time_t         _delta;
    time_t         _period_started;
  public:
    TMyLog(const String & fn)
      : _fn(fn), _orig_fn(fn), _sout(), _delta(0), _period_started(0)
      {
          //�������� ���� ��� - � ���� ������� - ��������
          if( !_fn.empty() ) {
            std::ifstream  fin(_fn.c_str());
            if(fin) {
                fin.seekg (0, std::ios::end);
                size_t fsize = fin.tellg();
                fin.seekg (0, std::ios::beg);

                if(fsize > _LOGFILE_MAX_SIZE ) ::unlink( _fn.c_str() );
            }
          }
      }
    virtual ~TMyLog(){  _fout.close(); }
    void close(){ _fout.close(); };
    void correct_time(time_t true_time){  _delta = (true_time > 0)? (true_time - Now()):(0); }
    void renew(const String& path);
    void reset(){
       close();
       _fn = _orig_fn;
    }

    void log_ok( const String &s) /**/
    {
        _sout = s;
         std::cout <<  _sout << std::endl;
        *this << _sout ;
    }
    void log_ok(time_t period_sec, const String &s) /**/
    {
       if(period_sec && _period_started  && (time(NULL) > (_period_started + period_sec)) ) _period_started = 0;

       if( (period_sec == 0) || (_period_started == 0) ) log_ok( s );

       if(period_sec && (_period_started == 0) ) _period_started = time(NULL);
    }
    void log_ok( LPCTSTR format, ...) /**/
    {
          va_list paramList;
          va_start(paramList, format);
          //String s;
          vprintf(_sout,format, paramList);  //     cat_vprintf(format, paramList);
          va_end(paramList);

          std::cout <<  _sout << std::endl;
          *this << _sout ;
    }
    
    #ifdef __TRUBLE__  //if use LOG_ERR
    //error: expected unqualified-id before numeric constant //?
    //All uppercase names are often used for preprocessor macros, which
    //doesn't respect namespace scopes. Therefore such names should
    //generally be avoided for everything else.
    #endif
    
    void log_err( const String &s) /**/
    {
        log_err( "%s", s.c_str() );
    }
    void log_err( LPCTSTR format, ...) /**/
    {
          va_list paramList;
          va_start(paramList, format);
          //String s;
          vprintf(_sout,format, paramList);  //     cat_vprintf(format, paramList);
          va_end(paramList);

          std::cerr <<  _sout << std::endl;
          *this << _sout ;
    }
  private:
    friend std::ofstream& operator << (TMyLog& rhl, const String &sout); //�� ����� ��� ���������


};

//------------------------------------------------------------------------------------------
inline
std::ofstream& operator << (TMyLog& rhl, const String &sout) //
{
    if( rhl._fn.empty() ) return rhl._fout;
    if( !rhl._fout.is_open() ) {
      //��������� ���� �� ������
      rhl._fout.open( rhl._fn.c_str(), std::ios_base::in | std::ios_base::out | std::ios_base::app);
      if( !rhl._fout ) THROW_FSTR(-1, "Can't open file '%s'", rhl._fn.c_str());

      //t1 rhl._fout.exceptions ( /*std::ofstream::eofbit | std::ofstream::failbit |*/ std::ofstream::badbit );
    }
    if( rhl._fout ) {
        //������ ������:
        rhl._fout << GetTime("[" __LOG_PREFIX ", %d.%m.%Y %T]", rhl._delta) << ": " << sout;
        rhl._fout << std::endl;
        //fout.close();
        if( !rhl._fout ) THROW_FSTR(-1, "Can't write to file '%s'", rhl._fn.c_str());
    }

    return rhl._fout;
}


//------------------------------------------------------------------------------------------
#ifdef __INDESIGN
class TExMyLog: public TMyLog
{
  private:
    String    _pref;
  public:
    TExMyLog(TMyLog const String& fn, _pref)

};
#endif

//------------------------------------------------------------------------------------------

template <typename T>
inline
void  sprintf_arr(String &sout, const String &Sh, T* Array, size_t Items, size_t Cols)
{
  //sout = "";
  String s;
  for(size_t i = 0; i < Items; ) {
      sprintf(s,"%04X: ", i );
      for(size_t col=0; (col < Cols) && (i < Items); col++, i++){
          cat_sprintf(s,Sh.c_str(), Array[i]);
      }
      cat_sprintf(sout,"%s\n",s.c_str());
  }
}
//------------------------------------------------------------------------------------------

template <typename T>
inline
void  sprintf_arr(String &sout, const String &Sh, T* Array, size_t Items)
{
  //sout = "";
  for(size_t i = 0; i < Items; i++){
    cat_sprintf(sout,Sh.c_str(), Array[i]);
  }
}

//------------------------------------------------------------------------------------------
#ifdef __INDESIGN
inline
std::ofstream& operator << (ofstream& os) //
{
  #ifndef __DEBUG
    std::cout << os;
  #else
  #endif
}
#endif

#endif

