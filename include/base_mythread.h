#ifndef __BASE_MYTHREAD_H
#define __BASE_MYTHREAD_H

#include <pthread.h>
#include <signal.h>
#include <sched.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
//#include <cxxabi.h>
#include <time.h>

#include <sys/time.h>
#include <sys/resource.h>

#include "include/my_except.h"
#include "include/mydebug.h"
#include "include/my_func.h"

#define handle_error_en(en, msg) \
   do { errno = en; perror(msg); /* exit(EXIT_FAILURE); */ } while (0)


class Tmutex
{
  private:
	::pthread_mutex_t 			_mutex;
  public:
    Tmutex()
	{
	    //_mutex = PTHREAD_MUTEX_INITIALIZER;
		::pthread_mutex_init(&_mutex, NULL);
	}
    virtual ~Tmutex()
	{
		::pthread_mutex_destroy(&_mutex);
	}
  public:
	void lock()
	{
  	  ::pthread_mutex_lock(&_mutex); /* _mutex.lock(); */ 
	}
	void unlock()
	{
  	  ::pthread_mutex_unlock(&_mutex); /* _mutex.lock(); */ 
	}
	bool trylock()
	{
	  //int pthread_mutex_trylock(pthread_mutex_t *mutex);
	  return (0 == ::pthread_mutex_trylock(&_mutex) );
	}
  private:
	::pthread_mutex_t*  get_mutex(){return &_mutex;}
	friend class Twait_condition;
};

static const long NSEC_PER_SEC = 1000000000L;

#ifndef unlike
  #define unlike(x) __builtin_expect(!!(x), 0)
  #define like(x) __builtin_expect(!!(x), 1)
#endif

static inline
void timespec_add_ns(struct timespec* a, long ns)
{
    ns += a->tv_nsec;
    while( ( ns >= NSEC_PER_SEC) ) { 
        ns -= NSEC_PER_SEC;
        a->tv_sec++;
    }
    a->tv_nsec = ns;
}


class Twait_condition
{
  private:
	::pthread_cond_t			_wait_cond;
  public:
	Twait_condition()
	{
		//_wait_cond = PTHREAD_COND_INITIALIZER;
		::pthread_cond_init (&_wait_cond, NULL);
	}
	virtual ~Twait_condition()
	{
		::pthread_cond_destroy(&_wait_cond);
	}
  public:
	void wakeAll()
	{
		//::pthread_cond_signal(&_wait_cond); //_queNotFull.wakeAll();
		::pthread_cond_broadcast(&_wait_cond);
	}
	void wake()
	{
		::pthread_cond_signal(&_wait_cond); //_queNotFull.wakeAll();
	}
	void wait(Tmutex* mutex)
	{
		::pthread_cond_wait(&_wait_cond, mutex->get_mutex());  //_queNotFull.wait(&_mutex);
	}
	bool wait_(Tmutex* mutex, unsigned long time_ms) //!TODO ������-�� ������ ����� (���� �� ���������)
	{
		struct timespec now;
		
		if(-1 == ::clock_gettime(CLOCK_REALTIME, &now) ) __MSG( __func__<< "(): " << strerror(errno) );
		timespec_add_ns( &now, time_ms * 1000);
		int rc;
		//int pthread_cond_timedwait(pthread_cond_t *restrict cond, pthread_mutex_t *restrict mutex, const struct timespec *restrict abstime);
		if( 0 != (rc = pthread_cond_timedwait(&_wait_cond, mutex->get_mutex(), &now)) ) {
		  if(rc != ETIMEDOUT) handle_error_en(rc, "wait cond" );
		  return false;
		}
		return true;
	}
};


#define __NEW_THREAD_PRIOR  (30)
#define __SCHED_POLICY      SCHED_RR /* SCHED_OTHER */ 
#define __NEW_PROC_PRIOR    (-18)
#define __USING_ATTR_THREAD

class Tbase_mythread
{
  protected:
	::pthread_t 					_thread;
  private:
    volatile bool 					_do_abort;
    //volatile bool 					_is_running;
	volatile bool 					_is_sleeping;
	Tstring 						_name; //��� info
	Tmutex                          _mutex;
	Twait_condition                 _in_sleep;
    my_logic_error                  _last_error;
	void 							*_status;
  public:
    explicit Tbase_mythread(const char* name = "noname")
	  : _thread(0), _do_abort(false), _is_sleeping(false), _name(name)
	{
	  _status = NULL;
	}
	virtual ~Tbase_mythread()
	{
		stop();
	}
  protected:
    inline 
    volatile bool CancellationPoint()
	{
		pthread_testcancel();
		if(_do_abort) return true;
		if(_is_sleeping) {
		  //::pthread_yield();
		  _mutex.trylock(); //��� ��� ������ ������� �� cancell �� �� ���� ��� �� ��������
		  _in_sleep.wait(&_mutex);
		  _mutex.unlock();
		}
		//nn pthread_testcancel();
		if(_do_abort) return true;
		  
		return false;
	}
	const Tstring& get_thread_name(){ return _name; }
  public:
	void abort()
	{ 
	  int rc;
	  //int pthread_cancel(pthread_t thread);
	  if ( isRunning() ) {
	     if( 0 != (rc = pthread_cancel(_thread)) )  handle_error_en(rc, _name.c_str() ); //���� �� ������� �� �������, �� ����� �� ��������� ������� �� �����
		 ::pthread_join(_thread, &_status);//		wait();
		 if (_status && (_status == PTHREAD_CANCELED) ) __MSG("thread '" << _name << "' cancelled...");
	  } 
	  //nn resume();
	}
	void abort_by_flag()
	{ 
	  //int pthread_cancel(pthread_t thread);
	  if ( isRunning() ) _do_abort = true;
	  resume();
	}
    template<typename T>
    void start(T *self , int *prior)
	{
	    if( isRunning() ) return;
	  
        _is_sleeping = false;
		_do_abort = false;
		//int pthread_create(pthread_t *thread, pthread_attr_t *attr, void *(*start_routine)(void *), void *arg);	
          __MSG("start new thread '" << _name << "'... process prior = " << ::getpriority(PRIO_PROCESS, 0));
          ::pthread_attr_t  * p_sched_attr = NULL;
		  
          #if defined ( __USING_ATTR_THREAD) && defined (TMS320DM365)
			  ::pthread_attr_t  sched_attr;
			  ::sched_param  	sched_param;
			  int 				sched_policy = __SCHED_POLICY;
		  
			  int min_prior = ::sched_get_priority_min(__SCHED_POLICY);
			  int max_prior = ::sched_get_priority_max(__SCHED_POLICY);
			  __MSG("min prior = " <<  min_prior);
			  __MSG("max prior = " <<  max_prior);

			  if( prior ) {
				::pthread_attr_init(&sched_attr);
				sched_param.sched_priority = ::mBound(min_prior, *prior, max_prior);
				__MSG("set prior = " << sched_param.sched_priority );

				if( 0 > ::pthread_attr_setschedparam  (&sched_attr, &sched_param) )           __MSG("err: " << strerror(errno));
				if( 0 > ::pthread_attr_setinheritsched(&sched_attr, PTHREAD_EXPLICIT_SCHED) ) __MSG("err: " << strerror(errno));
				if( 0 > ::pthread_attr_setschedpolicy (&sched_attr, __SCHED_POLICY) )         __MSG("err: " << strerror(errno));

				p_sched_attr = &sched_attr;
			  }
		  #endif
          
          //printf("2) self addr = %p\n", (void *) this);
          if( 0 > ::pthread_create(&_thread, p_sched_attr, run<T>, (void *) self) ) {
               THROW_FSTR(-1, "Can't create pthread: %s", strerror(errno));
          }
		  
		  #if defined ( __USING_ATTR_THREAD) && defined (TMS320DM365)
			bool need_info = true;
			if( need_info ){
				  if( 0 > ::pthread_getschedparam(_thread, &sched_policy, &sched_param) ) __MSG("err: " << strerror(errno));
			
				__MSG("thread policy = " << sched_policy);
				__MSG("thread prior = " <<  sched_param.sched_priority);
			}
		  #endif
	}
    inline void pause() { _is_sleeping = true; }
	inline bool is_sleeping() { return _is_sleeping; }
	inline void resume() {
		  _is_sleeping = false;
		  _in_sleep.wakeAll();
	}
	inline const my_logic_error&  LastError(){ return _last_error; }
    void SetLastError(const my_logic_error& e ){ _last_error = e; }
    
	bool wait()
	{
		if( isRunning() ) {
			::pthread_join(_thread, &_status);//		wait();
			if (_status && (_status == PTHREAD_CANCELED) ) __MSG("thread '" << _name << "' cancelled...");
		}
		return true;
	}
	bool wait_(unsigned long time_ms) //!TODO ������-�� ������ ����� (���� �� ���������)
	{
		if( !isRunning() )  return true;
		
		struct timespec now;
		
		if(-1 == ::clock_gettime(CLOCK_REALTIME, &now) ) __MSG( __func__<< "(): " << strerror(errno) );
		timespec_add_ns( &now, time_ms * 1000);
		int rc;
		// int pthread_timedjoin_np(pthread_t thread, void **retval, const struct timespec *abstime);
		if( 0 != (rc =pthread_timedjoin_np(_thread, &_status, &now) ) ) {
		  if(rc != ETIMEDOUT) handle_error_en(rc, "timedjoin" );
		  return false;
		}
		if (_status && (_status == PTHREAD_CANCELED) ) __MSG("thread '" << _name << "' cancelled...");
		return true;
	}
	void stop()
	{ 
	  abort();
	  if ( ! wait() ) __MSG(__func__ << "():Timeout wait thread cancel");
	}
    bool isRunning(){ return ( _thread && ( ::pthread_kill(_thread, 0 ) == 0) ); }
  protected:
    //static void* run(void * self) = 0;
  public:
	//----------------------------------------------------------------------------
    template<typename T>
	static void*
	run(void */* T* */self)
	{
		T* _self = (T*) self;
		Tstring sout = "thread '" + _self->_name + "':";
		//v1 ::setpriority(PRIO_PROCESS, 0, -10); //set to highest priority
		//pthread_cleanup_push(cleanupHandler, NULL);

		try {
			_self->execute();
		}
		catch (my_logic_error &e) {
			__MSG( sout << e.what() );
			_self->SetLastError(e);
		}
		catch (std::exception &e){ //������� �� ��� ������
			__MSG( sout << e.what() );
			_self->SetLastError(my_logic_error(-1, e.what()));
		}
#ifdef __VER_FOR_GCC_4_3
		catch (abi::__forced_unwind&) {
			//nn __MSG("thread '" << _self->_name << "' cancelled...");
			throw;
		}
		catch(...){
			__MSG( sout << "unknown error" );
			_self->SetLastError( my_logic_error(-1, "unknown error") );
		}
#else
		catch(...){ //��� cancel ����� �������� ����������
			__DMSG( sout << "unknown error" );
			throw;
		}
#endif		
		//pthread_cleanup_pop(0);
		
		__MSG("thread '" << _self->_name << "' finished...");
		//if(res) _self->send_end_session();

		::pthread_exit(NULL);
	}
  
};



class Tmutex_locker
{
	private:
	   Tmutex		*_mutex;
	public:
	  Tmutex_locker(Tmutex* mutex)
	   : _mutex(mutex)
	  {
		_mutex->lock();
	  }
      virtual ~Tmutex_locker()
	  {
		_mutex->unlock();
	  }
};


class Tsemaphore
{
  private:
	::sem_t			_sem_id;
  public:
	Tsemaphore()
	{
		if ( 0 > ::sem_init(&_sem_id, 0, 0) ) THROW_STR(-1, "semaphore creation failed"); 		
	}
    virtual ~Tsemaphore()
	{
		::sem_destroy(&_sem_id);
	}
	void post(){
	      ::sem_post(&_sem_id); 
	}
	void wait(){
	      ::sem_wait(&_sem_id); 
	}
	#ifdef __NEED
	void close(){
	      ::sem_close(&_sem_id); 
	}
	void open(){
	      ::sem_open(&_sem_id); 
	}
	#endif
};

#include <sys/time.h>
#include <sys/resource.h>

class Tup_priority
{
  public:
	Tup_priority(int mnice)
	: _mnice(mnice)
	{
             ::setpriority(PRIO_PROCESS, 0, -20);
//	  _prev_prior = ::getpriority(PRIO_PROCESS, 0);
//      __MSG("curr prior = " << _prev_prior << " set nice to: " << _mnice);
//      errno = 0;
//      if( (-1 == ::nice(_mnice)) && (errno != 0) ) __MSG("Can't set nice: "  << ::strerror(errno));
//      __MSG("new prior = " << ::getpriority(PRIO_PROCESS, 0) );
        }

	~Tup_priority()
	{
            ::setpriority(PRIO_PROCESS, 0, -20);
//      __MSG("curr prior = " << ::getpriority(PRIO_PROCESS, 0) << " set prior to: " << _prev_prior);
//      errno = 0;
//      if( (-1 == ::setpriority(PRIO_PROCESS, 0, _prev_prior)) && (errno != 0) ) __MSG("Can't set nice: "  << ::strerror(errno));
//      __MSG("new prior = " << ::getpriority(PRIO_PROCESS, 0) );
    }
  private:
	int _mnice;
	int _prev_prior;
};

template <typename T = int32_t>
class TAtomicInt
{
private:
  T		  				_val;
  mutable Tmutex 		_mutex;
public:
  explicit TAtomicInt(T val = 0)
   : _val(val)
  {}
  TAtomicInt& set(T val)
  {
	_mutex.lock();
	_val = val;
	_mutex.unlock();
	
	return *this;
  }
  T get() const 
  {
	Tmutex_locker ml(&_mutex);
	return _val;
  }
  TAtomicInt& operator=(const T val)
  {
	return set(val);
  }
};

#ifdef __INDESIGN
template <typename T = int32_t>
inline
T& operator= (T &lhs, const TAtomicInt<T> &rhs)
{
  lhs = rhs.get();
  return lhs;
}

if(pthread_kill(the_thread, 0) == 0)
{
    /* still running */
}

#endif

#endif

