#ifndef LibDef_h__
#define LibDef_h__

#ifdef WIN32
#pragma warning(disable : 4996)
#define inline _inline
#endif

/* enable structure packing */
#if defined(__GNUC__)
  #define	PACKED	__attribute__((packed))
#else //defined _MSC_VER
  #pragma   pack(push, 1)
  #define	PACKED
#endif

 
/* unsigned quantities */
typedef unsigned long long	Uint64;
typedef unsigned int   		Uint32;
typedef unsigned short 		Uint16;
typedef unsigned char		Uint8;

/* signed quantities */
typedef long long		Int64;
typedef int             Int32;
typedef short           Int16;
typedef char            Int8;

 
//#define Uint16 unsigned Int16
//#define Int16 Int16
//#define Uint16 Uint16
//#define Uint32 unsigned long
//#define Int32 long
//#define size_t Uint32
//#define Uint8 unsigned char
//#define BYTE Uint8
//#define Int64 long long
//#define ULONGLONG unsigned Int64

#ifndef TRUE
  //#define NULL 0
  #define FALSE 0
  #define TRUE 1
#endif
//
#define BOOL int

#ifdef WIN32
#else

#ifdef __IFNEED
	#define abs(a) ((a) >= 0 ? (a) : -(a)) 
	#define max(a,b) ((a) >= (b) ? (a) : (b)) 
	#define min(a,b) ((a) < (b) ? (a) : (b))
	#define Sleep(a)	usleep(a * 1000)
#endif

#endif


#ifndef SECTOR_SIZE
#define SECTOR_SIZE 512
#endif

#ifndef BZERO
#define BZERO(buf) memset(&(buf), 0, sizeof(buf))
#endif


#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif

#ifndef SOCKET
#define SOCKET Int32
#endif

//
//#ifndef DBGERR
//#define DBGERR printf
//#endif

#ifndef E_FAIL
#define E_FAIL 0x80004005L
#endif

#ifndef S_OK
#define S_OK 0L
#endif

#ifndef S_FALSE
#define S_FALSE 1L
#endif

#ifndef E_ERR
#define E_ERR -1L
#endif

#define SETUINT16(lob, hib)      (((Uint16)(lob) & 0xffu) | (((Uint16)(hib) & 0xffu) << 8))
#define SETUINT32(low, hiw)     (((Uint32)(low) & 0xffffu) | (((Uint32)(hiw) & 0xfffful) << 16))
#define LOUINT32(l)           ((Uint32)(((Uint64)(l)) & 0xfffffffful))
#define HIUINT32(l)           ((Uint32)((((Uint64)(l)) >> 32) & 0xfffffffful))
#define LOUINT16(l)           ((Uint16)(((Uint32)(l)) & 0xfffful))
#define HIUINT16(l)           ((Uint16)((((Uint32)(l)) >> 16) & 0xffffu))
#define LOUINT8(l)           ((Uint8)(((Uint16)(l)) & 0xffu))
#define HIUINT8(l)           ((Uint8)((((Uint16)(l)) >> 8) & 0xffu))


//#define SETUINT16(lob, hib)      (((Uint16)(lob) & 0xff) | (((Uint16)(hib) & 0xff) << 8))
//#define SETUINT32(low, hiw)     (((Uint32)(low) & 0xffff) | (((Uint32)(hiw) & 0xffff) << 16))
//#define LOUINT32(l)           ((Uint32)(((Uint64)(l)) & 0xffffffff))
//#define HIUINT32(l)           ((Uint32)((((Uint64)(l)) >> 32) & 0xffffffff))
//#define LOUINT16(l)           ((Uint16)(((Uint32)(l)) & 0xffff))
//#define HIUINT16(l)           ((Uint16)((((Uint32)(l)) >> 16) & 0xffff))
//#define LOUINT8(l)           ((Uint8)(((Uint16)(l)) & 0xff))
//#define HIUINT8(l)           ((Uint8)((((Uint16)(l)) >> 8) & 0xff))

//#define MAKELONG(a, b)      ((LONG)(((Uint16)(((Uint32_PTR)(a)) & 0xffff)) | ((Uint32)((Uint16)(((Uint32_PTR)(b)) & 0xffff))) << 16))

//#define MAKEWORD(a, b)      ((Uint16)(((BYTE)(((Uint32_PTR)(a)) & 0xff)) | ((Uint16)((BYTE)(((Uint32_PTR)(b)) & 0xff))) << 8))
//#define MAKELONG(a, b)      ((LONG)(((Uint16)(((Uint32_PTR)(a)) & 0xffff)) | ((Uint32)((Uint16)(((Uint32_PTR)(b)) & 0xffff))) << 16))
//#define LOWORD(l)           ((Uint16)(((Uint32_PTR)(l)) & 0xffff))
//#define HIWORD(l)           ((Uint16)((((Uint32_PTR)(l)) >> 16) & 0xffff))
//#define LOBYTE(w)           ((BYTE)(((Uint32_PTR)(w)) & 0xff))
//#define HIBYTE(w)           ((BYTE)((((Uint32_PTR)(w)) >> 8) & 0xff))

enum ROSS_AUDIO_ID 
{
	audio_mono1		= 0x000a,
	audio_mono2		= 0x010a,
	audio_stereo	= 0x001a,
	audio_mono		= 0x000a,
	audio_mon_stereo	= 0x003a,
	audio_mon_mono		= 0x002a,
};

#define CRYPT_KEY_SIZE 32

enum ROSS_AUDIO_CRYPT_ID 
{
	audio_crypt_none	= 0x0,
	audio_crypt_user	= 0x1,
	audio_crypt_vendor	= 0x2,
	audio_crypt_combo	= 0x3,
};

//����������� ���������� 1, 3, 4, 5, 8
enum ADPCM_COMPRESSION 
{
	adpcm_1			= 0x0001,
	adpcm_3			= 0x0003,
	adpcm_4			= 0x0004,
	adpcm_5			= 0x0005,
	adpcm_8			= 0x0008,
};

#define BASE_SAMPLE_RATE 32000

//������ ������   3,6��� - 4,  6.3��� - 2, 16��� -  1
enum AUDIO_BAND 
{
	audio_band_3600			= 0x0004,
	audio_band_6300			= 0x0002,
	audio_band_16000		= 0x0001,
};

enum RECORDING_STATUS 
{
	mon_flag			= 0x0001,
	rec_flag			= 0x0002,
	rec_and_mon_flag	= 0x0003,
	copy_data_flag		= 0x0010,
	copy_peak_flag		= 0x0020,
	erase_flag			= 0x0100,
};

typedef struct Tlocal_recorder_status_s
{
	//���������� � �������� ������� ()
	uint16_t _is_monitoring	: 1; //0: 
	uint16_t _is_recording		: 1; //1: 
	uint16_t _reserv			: 2; //2: 
	uint16_t _is_copy_data		: 1; //4: 
	uint16_t _is_copy_peak		: 1; //5: 
	uint16_t _reserv2			: 2; //6: 
	uint16_t _is_erasing		: 1; //8: 
	uint16_t _reserv3			: (16-9); //9: empty
} __attribute__((packed)) Tlocal_recorder_status;


//enum CR2_COMMAND
//{
//	cr2cmdSaveParam			= 0x5503,	// �������� ���� ���������� 
//	cr2cmdReadParam			= 0x5504,	// ������� ���� ���������� 
//	cr2cmdReadStatus		= 0x5507,	// ������� ������ (������� �������) 
//	cr2cmdSaveStatus		= 0x5502,	// �������� ���� ���������� 
//
//	cr2cmdStartMonitoring	= 0x5520,	// ������ ���������� 
//	cr2cmdStopMonitoring	= 0x5522,	// ��������� ���������� 
//	cr2cmdStartRecord		= 0x5521,	// ������ ������ 
//	cr2cmdStopRecord		= 0x5523,	// ��������� ������ 
//
//	cr2cmdReadData			= 0x5524,	// ������� ������ �� ������� ������ 
//	cr2cmdReadPeak			= 0x5525,	// ������� ������ �� ������� ��������� 
//
//	cr2cmdClear				= 0x5526,	// ������� �������� 
//	cr2cmdFullErase			= 0x5527,	// ������ �������� 
//
//	//c3dcmdSaveAlias			= 0x15,	// �������� ��������� */
//	//c3dcmdReadAlias			= 0x16,	// ������ ��������� */	
//};

typedef struct INFO_FLASH
{
	Uint64 ullStartSeek;
	Uint64 ullCountByte;
	Uint64 ullCurrentSeek;
	Uint64 ullReserved; // ��� ������������
} INFO_FLASH;

typedef struct vUint16
{
	Uint32 cwLen;	// ����� � ������
	Uint32 cwIndex;
	Uint16 *pData;
} vUint16;

typedef struct vInt16
{
	Uint32 cwLen;	// ����� � ������
	Uint32 cwIndex;
	Int16 *pData;
} vInt16;

typedef struct vInt8
{
	Uint32 cbLen;	// ����� � ������
	Uint32 cbIndex;
	Uint8 *pData;
} vUint8;

// ����� ���� ����������� ������ � ������
#define PACKET_DATA_SIZE 248

// ����� ���� � ������ ������
#define  EXCHANGE_PACKET_SIZE 512

enum AUDIO_HEADER_MASK 
{
	audio_band_mask	= 0x07,
	audio_compress_mask	= 0x0f,
	audio_denc_mask	= 0x0fff,
	audio_crypt_user_mask	= 0x80,
	audio_crypt_vendor_mask	= 0x80,
	audio_crypt_newver_mask = 0x20,
};

struct ROSS_AUDIO_HEADER
{
	Uint8 ucPacketID;
	Uint8 ucChannel;
	Uint8 ucCompressL;
	Uint8 ucBandL;
	Uint8 ucCompressR;
	Uint8 ucBandR;
	Uint8 ucDencL0;
	Uint8 ucDencL8;
	Uint8 ucDencR0;
	Uint8 ucDencR8;
	Uint8 ucTime16;
	Uint8 ucTime24;
	Uint8 ucTime0;
	Uint8 ucTime8;
	Uint8 ucPeakL;
	Uint8 ucPeakR;
} PACKED;

struct AUDIO_PACKET
{
	ROSS_AUDIO_HEADER audioHeader;
	Uint16 awAudioData[PACKET_DATA_SIZE];
} PACKED;

struct ROSS_HEAD_AUDIO_PACKET
{
	Uint8 ucPacketID;
	Uint8 ucChannel;
	Uint8 ucSig1;
	Uint8 ucSig2;
	Uint8 ucSig3;
	Uint8 ucSig4;
	Uint8 ucSig5;
	Uint8 ucSig6;
	Uint8 ucRsrv1;
	Uint8 ucRsrv2;
	Uint8 ucTime16;
	Uint8 ucTime24;
	Uint8 ucTime0;
	Uint8 ucTime8;
	Uint8 ucRsrv3;
	Uint8 ucRsrv4;
	Uint8 aucData[EXCHANGE_PACKET_SIZE - 16];
} PACKED;


struct ROSS_PEAK_HEADER
{
	Uint8 ucPacketID;
	Uint8 ucChannel;
	Uint8 ucPeakL;
	Uint8 ucPeakR;
	Uint8 ucTime16;
	Uint8 ucTime24;
	Uint8 ucTime0;
	Uint8 ucTime8;
} PACKED;

struct ROSS_PEAK_PACKET
{
	ROSS_PEAK_HEADER Peaks[64];
} PACKED;

#if !defined (__GNUC__)
  #pragma   pack(pop)
#endif


#endif // LibDef_h__


