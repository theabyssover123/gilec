#ifndef __MY_IO_H
#define __MY_IO_H

#include <time.h> //for nsleep
#include "io.h" //path def by -I 

/* Clear and set bits in one shot.  These macros can be used to clear and
 * set multiple bits in a register using a single read-modify-write.  These
 * macros can also be used to set a multiple-bit bit pattern using a mask,
 * by specifying the mask in the 'clear' parameter and the new bit pattern
 * in the 'set' parameter.
 */

#define clrsetbits(type, addr, clear, set) \
	out_##type((addr), (in_##type(addr) & ~(clear)) | (set))

#ifdef __powerpc64__
#define clrsetbits_be64(addr, clear, set) clrsetbits(be64, addr, clear, set)
#define clrsetbits_le64(addr, clear, set) clrsetbits(le64, addr, clear, set)
#endif

#define clrsetbits_be32(addr, clear, set) clrsetbits(be32, addr, clear, set)
#define clrsetbits_le32(addr, clear, set) clrsetbits(le32, addr, clear, set)

#define clrsetbits_be16(addr, clear, set) clrsetbits(be16, addr, clear, set)
#define clrsetbits_le16(addr, clear, set) clrsetbits(le32, addr, clear, set)

#define clrsetbits_8(addr, clear, set) clrsetbits(8, addr, clear, set)

/* enable structure packing */
#if defined(__GNUC__)
  #define	PACKED	__attribute__((packed))
#else
  #pragma pack(1)
  #define	PACKED
#endif

#define __N_GPIO_0     0 
#define __N_GPIO_1     1 
#define __N_GPIO_2     2 
#define __N_GPIO_3     3 
#define __N_GPIO_4     4 
#define __N_GPIO_5     5 
#define __N_GPIO_6     6 
#define __N_GPIO_7     7 
#define __N_GPIO_8     8 
#define __N_GPIO_9     9 
#define __N_GPIO_10    10
#define __N_GPIO_11    11
#define __N_GPIO_12    12
#define __N_GPIO_13    13
#define __N_GPIO_14    14
#define __N_GPIO_15    15
#define __N_GPIO_16    16
#define __N_GPIO_17    17
#define __N_GPIO_18    18
#define __N_GPIO_19    19
#define __N_GPIO_20    20
#define __N_GPIO_21    21
#define __N_GPIO_22    22
#define __N_GPIO_23    23
#define __N_GPIO_24    24
#define __N_GPIO_25    25
#define __N_GPIO_26    26
#define __N_GPIO_27    27
#define __N_GPIO_28    28
#define __N_GPIO_29    29
#define __N_GPIO_30    30
#define __N_GPIO_31    31

#define __SHIFT_GPIO(N_GPIO)  (31-(N_GPIO)) /*val== 1<<shift*/
                       

struct gpio32_bits_t {
	u32 gpio0 :1;		
	u32 gpio1 :1;		
	u32 gpio2 :1;		
	u32 gpio3 :1;		
	u32 gpio4 :1;		
	u32 gpio5 :1;		
	u32 gpio6 :1;		
	u32 gpio7 :1;		
	u32 gpio8 :1;		
	u32 gpio9 :1;		
	u32 gpio10:1;		
	u32 gpio11:1;		
	u32 gpio12:1;		
	u32 gpio13:1;		
	u32 gpio14:1;		
	u32 gpio15:1;		
	u32 gpio16:1;		
	u32 gpio17:1;		
	u32 gpio18:1;		
	u32 gpio19:1;		
	u32 gpio20:1;		
	u32 gpio21:1;		
	u32 gpio22:1;		
	u32 gpio23:1;		
	u32 gpio24:1;		
	u32 gpio25:1;		
	u32 gpio26:1;		
	u32 gpio27:1;		
	u32 gpio28:1;		
	u32 gpio29:1;		
	u32 gpio30:1;		
	u32 gpio31:1;		
} PACKED;

union u_gpio32_t {
  u32                   _val;
  struct gpio32_bits_t  _bits;

} PACKED;


// int nanosleep(const struct timespec *rqtp, struct timespec *rmtp);
// struct timespec
// {
//   __time_t tv_sec;		/* Seconds.  */
//   long int tv_nsec;		/* Nanoseconds.  */
// };
inline
int nsleep(long int nanoSec)
{
  struct timespec ts= {0,0};
  ts.tv_nsec  = nanoSec; //���� ������� = nano sec
  
  // int nanosleep(const struct timespec *rqtp, struct timespec *rmtp);
  return nanosleep(&ts, NULL);
}



#endif
