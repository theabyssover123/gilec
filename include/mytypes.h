#ifndef __MY_TYPES_H
#define __MY_TYPES_H

#if 0

#include <stdio.h>
#include <bits/types.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>

#endif

#include <stdint.h>
#include <stdexcept>
#include <sys/socket.h>
#include <unistd.h> //for close


//#include "mystring.h"
//#include "mylog_class.h"


#ifndef BZERO
    #define BZERO(buf) memset(&(buf), 0, sizeof(buf))
#endif


#if !defined(__BOOLEAN) && !defined (TRUE)
    #define __BOOLEAN
    typedef      int BOOL;
    #define TRUE   1
    #define FALSE  0
#endif



#define MAX_STR_LEN     128
#define __NUM(arr)  (sizeof(arr)/ sizeof(arr[0]))
#define __Case     break;case
#define __Default  break;default

#define HiByte(Word)                     ((Word)>>8)
#define LoByte(Word)                     ((Word) & 0xff)

typedef char            TCHAR;
typedef const char *    LPCTSTR;
typedef const char *    LPCSTR;
typedef       char *    LPSTR;
typedef unsigned int    UINT;
typedef unsigned char   UCHAR;

#define __LARGE_SIZE_MODEL
#ifdef __LARGE_SIZE_MODEL
  typedef uint64_t			   Size_t; //equ uint64_t
#else
  typedef size_t               Size_t; //equ ???
#endif




#ifdef __cplusplus

#ifndef  __USE_QT_EXCEPTIONS
    class my_logic_error : public std::runtime_error
    {
      public:
        typedef int  error_code;
        //system_error(error_code __ec = error_code())
        //  : runtime_error(""), _M_code(__ec) { }
        my_logic_error()
          : std::runtime_error("No errors"), _M_code(0) { }
    
        my_logic_error(int __ec, const std::string& __what)
          : std::runtime_error(__what), _M_code(__ec) { }
          
        my_logic_error(int __ec, const char* __what)
          : std::runtime_error(__what), _M_code(__ec) { }

        my_logic_error(const my_logic_error& rhs)
		  : std::runtime_error(rhs.what()), _M_code(rhs._M_code) { }

        virtual ~my_logic_error() throw(){};

        const error_code&
        code() const throw() { return _M_code; }
      private:
        error_code 	_M_code;
    };

    class my_system_error : public my_logic_error
    {
      public:
        my_system_error(int __ec, const std::string& __what)
          : my_logic_error(__ec, __what) { }
        virtual ~my_system_error() throw(){};
    };
#else
	#include <QtCore>
	
	#ifdef QT_NO_EXCEPTIONS
	  #error  myerror:  no exceptions
	#endif  

	class my_logic_error : public QtConcurrent::Exception
	{
	 public:
		typedef int  error_code;
	 public:
		void raise() const { throw *this; }
		Exception *clone() const { return new my_logic_error(*this); }
		
	 public:
		my_logic_error(int __ec, const std::string& __what)
		  : _M_what(__what), _M_code(__ec) { }
		my_logic_error(const my_logic_error& rhs)
		  : _M_what(rhs._M_what), _M_code(rhs._M_code) { }
		virtual ~my_logic_error()throw (){}
        const error_code&
        code() const throw() { return _M_code; }
		const std::string&  what(){return _M_what; }
	 private:
			std::string  _M_what;
			error_code 	 _M_code;
	};
	
#endif


#endif

#ifdef __cplusplus
    //---------------------------------------------------------------------------
    //������ try __finally �� ��������� �� ���������� �++
    class TAutoCloseSocket {
        int       _socket;
      public:
        TAutoCloseSocket(int  socket)
          : _socket(socket){}
        ~TAutoCloseSocket(){
          // ��� ��������������� ������ ���� ������� (���� � ������ ����������)
          shutdown(_socket, SHUT_RDWR);
          close(_socket);
        }
    };

    //---------------------------------------------------------------------------
    //������ try __finally �� ��������� �� ���������� �++
    class TAutoClose {
        int       _fd;
      public:
        TAutoClose(int  fd)
          : _fd(fd){}
        ~TAutoClose(){
          // ��� ��������������� ������ ���� ������� (���� � ������ ����������)
          close(_fd);
        }
    };

	
#endif

typedef void  (*Tset_progress)(size_t pcnt); //����������� �������


#endif
