#ifndef __MY_FUNC_H
#define __MY_FUNC_H

#include <stdio.h>
//#include <string.h>
//#include <getopt.h> //POSIX
#include <time.h>
#include <fstream>
#include <iostream>
#include <errno.h>
#include <iterator>
#include <vector>

#include "my_except.h"
//#include "mylog_class.h"
//------------------------------------------------------------------------------
//!TODO then use regv
#define TIME_PATTERN  "%d/%d/%d %d:%d:%d" // DD/MM/YYYY  HH:MM:SS

//------------------------------------------------------------------------------
#define PERIOD_PATTERN  "%d:%d:%d" // HH:MM:SS
#define __CHECK_PATTERN

inline bool
period_to_time(time_t *dest_tm, LPCSTR inp_str)
{

    struct tm _tm_now;
    time_t _now;

    time(&_now); // �������� ��� �����
    _tm_now = *gmtime(&_now); //��������� ��� (����� � �� UTC) � ����������

    //������ ������ ������� � ��������
    _tm_now.tm_hour =
    _tm_now.tm_min  =
    _tm_now.tm_sec  = 0;

#ifdef __VER1
    struct tm _tm = {
        .tm_year = 0,
        .tm_mon  = 0,
        .tm_mday = 1,
        .tm_isdst = -1
    };
#else
    struct tm _dest = _tm_now;
#endif

    int res = sscanf(inp_str, PERIOD_PATTERN,
                      &_dest.tm_hour,
                      &_dest.tm_min,
                      &_dest.tm_sec
                );
    #ifdef __CHECK_PATTERN
        if(res < 3) {
          //this err exit
          return false;
        }
    #endif

    //!TODO make checking

    *dest_tm = mktime(&_dest) -  mktime(&_tm_now); //difftime(mktime(&_dest) ,  mktime(&_tm_now));
    return true;
}
//------------------------------------------------------------------------------
inline int
sleep_ms(long mSec)
{

  struct timeval tv;
  tv.tv_sec  = 0;
  tv.tv_usec = mSec * 1000;

  //int select(int n, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);
  return select(0, NULL, NULL, NULL, &tv);

}

//------------------------------------------------------------------------------
inline
bool fill_zero_file(const String & fn)
{
  // create a read/write file-stream object on char
  // and attach it to an ifstream object
  std::ifstream fin(fn.c_str(), std::ios_base::in | std::ios_base::out /*| std::ios_base::trunc */ | std::ios_base::binary);
  if(!fin) THROW_FSTR(-1, "Can't fill sero file: \"%s\": %s", fn.c_str(), strerror(errno));
  // tie the ostream object to the ifstream object
  std::ostream  fout( fin.rdbuf() );

  //std::ofstream fout(fn.c_str(), std::ios_base::out | std::ios_base::binary);
  if(fin && fout){
      fin.seekg (0, std::ios::end);
      size_t fsize = fin.tellg();
      fin.seekg (0, std::ios::beg);
      //fout.write()
      for(size_t cnt =0; /*!fin.eof() &&*/ (cnt < fsize) ; cnt++){  fout.put(0); }
  }

  return true;
}
//------------------------------------------------------------------------------
#ifdef __VER1
inline void
copy_file(const String & from_fn, const String & to_fn)
{
  try {
      // create a read/write file-stream object on char
      // and attach it to an ifstream object
      std::ifstream fin( from_fn.c_str() );
      std::ofstream fout( to_fn.c_str() );
      if(!fin)  THROW_FSTR(-1, "Can't open file: \"%s\": %s", from_fn.c_str(), strerror(errno));
      if(!fout) THROW_FSTR(-1, "Can't open file: \"%s\": %s",   to_fn.c_str(), strerror(errno));

      fin.exceptions  ( /*std::ifstream::eofbit | std::ifstream::failbit |*/ std::ifstream::badbit );
      fout.exceptions ( /*std::ifstream::eofbit | std::ifstream::failbit |*/ std::ifstream::badbit );

      if(fin && fout){
          std::istreambuf_iterator<char> eos;               // end-of-range iterator
          std::istreambuf_iterator<char> iit ( fin.rdbuf() ); // in iterator
          std::ostreambuf_iterator<char> oit ( fout.rdbuf() ); // out iterator
          //OutputIterator copy ( InputIterator first, InputIterator last, OutputIterator result )
          std::copy(iit, eos, oit);
      }
      fin.close();
      fout.close();
  }
  catch (std::exception &e){
    THROW_STR(-1, e.what());
  }

}
#else

inline void
copy_file(const String & from_fn, const String & to_fn)
{

  std::ifstream fin ( from_fn.c_str(), std::ifstream::binary);
  std::ofstream fout( to_fn.c_str()  , std::ofstream::binary);
  if(!fin)  THROW_FSTR(-1, "Can't open file: \"%s\": %s", from_fn.c_str(), strerror(errno));
  if(!fout) THROW_FSTR(-1, "Can't open file: \"%s\": %s",   to_fn.c_str(), strerror(errno));

  // get size of file
  fin.seekg(0, std::ifstream::end);
  long size = fin.tellg();
  fin.seekg(0);

  // allocate memory for file content
  std::vector<char> buffer(size);

  if(
      // read content of infile
      !fin.read   ( &buffer[0], size) ||
      // write to outfile
      !fout.write ( &buffer[0], size)
    ) THROW_FSTR(-1, "Can't write file: %s",  strerror(errno));


  fin.close();
  fout.close();

}

#endif

//------------------------------------------------------------------------------
inline bool
str_to_time(time_t *dest_tm, LPCSTR inp_str)
{
    struct tm _tm;
    BZERO(_tm);

    int res = sscanf(inp_str, TIME_PATTERN,
                      &_tm.tm_mday,
                      &_tm.tm_mon,
                      &_tm.tm_year,
                      &_tm.tm_hour,
                      &_tm.tm_min,
                      &_tm.tm_sec
                );
    #ifdef __CHECK_PATTERN
        if(res < 6) {
          //this err exit
          return false;
        }
    #endif
    //!TODO make checking
    _tm.tm_year -= 1900;
    _tm.tm_mon--;
    _tm.tm_isdst = -1;
    *dest_tm = mktime(&_tm);

    return true;
}
//------------------------------------------------------------------------------
template <typename T>
inline T
mBound(const T vMin, const T val, const T vMax)
{
  return std::min( std::max(val, vMin), vMax);
}

#endif
