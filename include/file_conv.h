#ifndef __FILE_CONV_H
#define __FILE_CONV_H

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h> //for mkdir
#include <unistd.h>
#include <errno.h> //for mkdir
#include <string.h>



#include "mystring.h"
//#include "my_env.h" //for AI_CATASTROFIC
#include "my_except.h"
//#include <string>

#ifndef   AI_CATASTROFIC
  #define AI_CATASTROFIC  -1010
#endif

//-----------------------------------------------------
inline String 
get_file_name(const String& fullname)
{
  #ifdef __VER1
      //const String pattern("(?<=[/^])(.*)$");
      char * p = strrchr((char *) fullname.c_str(), '/');
      if(p) return String( &p[1] );
      else return fullname;
  #else
      size_t found = fullname.find_last_of("/\\");
      if(found == String::npos) found = (size_t) -1;
      
      return fullname.substr(found+1);
  #endif
}
//-----------------------------------------------------
inline String 
get_file_name_woext(const String& fullname)
{
  size_t found = fullname.find_last_of("/\\");
  size_t found2 = fullname.find_last_of(".");
  if(found == String::npos) found = (size_t) -1;
  if(found2 == String::npos) found2 = fullname.size();
  
  return fullname.substr(found+1, found2);
}
//-----------------------------------------------------
inline
String get_file_path(const String& fullname) //!TODO ��������� (� ���� npos?)
{
  size_t found = fullname.find_last_of("/\\");
  return (found != String::npos)? fullname.substr(0,found):("");
}
//-----------------------------------------------------
inline
String get_file_ext(const String& fullname) //re:  ".ext"
{
  size_t found = fullname.find_last_of(".");
  return (found != String::npos)? fullname.substr(found):("");
}
//-----------------------------------------------------
// ������, ��� ����� ���� ������ ����� ������������� � ��� ������������ ������� ����������
// �������� cd /////home == cd /home ��� cd /home///root == cd /home/root
inline bool 
get_file_path(String& dst, const String& fullname, int n_sub) //in:  "sub0/sub1/sub2/fname.ext" re(0): sub0  //subst - ����� �������� ����������
{
  //ex1: /dir1/dir2/filename sub0="" <-curr dir
  //ex2: dir1/dir2/filename sub0=dir1
  //ex3: ./dir1/dir2/filename sub0=. <-curr dir
  //ex4: ../dir1/dir2/filename sub0=..
  //ex5: /filename sub0="" <-curr dir
  //ex6: filename  sub0="" <-curr dir
  size_t found = fullname.find_first_of("/\\");
  for ( ; n_sub > 0; n_sub--){
    if( found == String::npos ) break;
    found = fullname.find_first_of("/\\", found+1);
  }
  if (found != String::npos) {
    dst = fullname.substr(0, found);
  }
  return (found != String::npos);
}
//-----------------------------------------------------------------------------
inline size_t
get_file_size(std::ifstream& fin)
{
	// get size of file
	fin.seekg(0, std::ifstream::end);
	size_t size = fin.tellg();
	fin.seekg(0);
	return size;
}
//-----------------------------------------------------------------------------
inline bool
check_file(const String& file)
{
	//�������� ��� �������
	struct stat _stat;
	//int stat(const char *file_name, struct stat *buf);
	int res = ::stat( file.c_str(), &_stat);
	
	return (res < 0)?(false):(true);
}
//-----------------------------------------------------------------------------
//����� ��������� ������������� ����� � ���� ��� - �������
inline void
check_folder(const String& folder, bool locase = false)
{
    if(folder.empty()) return;

    String fld = (locase)? str_tolower(folder) : folder;

    DIR * dp;
    if (NULL == (dp = opendir( fld.c_str() ))){
        //����� ��� ��� - ��������
        //!DONE ������� ��� ��������
        if(0 > mkdir(fld.c_str(), 0755))
		  THROW_FSTR(AI_CATASTROFIC, "mkdir(): can't make dir '%s': %s", fld.c_str(), ::strerror(errno));
    }
    if(dp) closedir(dp);
}
//-----------------------------------------------------------------------------
inline void
check_tree(const String& tree, bool locase = false)
{
  //����� ��������� ������������� ����� � ���� ��� - ������� ��� ������
  String folder    = tree + "/null";

  for(int i=0; true; i++) {         //in:  "sub0/sub1/sub2/fname.ext" re(0): sub0
      String subfolder;
      //bool get_file_path(String& dst, const String& fullname, int n_sub) //in:  "sub0/sub1/sub2/fname.ext" re(0): sub0  //subst - ����� �������� ����������
      if( !get_file_path(subfolder, folder, i) ) break;

      ::check_folder(subfolder, locase);
  }
}

//-----------------------------------------------------
#ifdef __IN_BUILD
inline
String trim_str(const String& str) //������� ������������ �������
{

}
#endif

//-------------------------------------------------------

inline void 
SplitFilename (const std::string& str)
{
  size_t found;
  std::cout << "Splitting: " << str << std::endl;
  found=str.find_last_of("/\\");
  std::cout << " folder: " << str.substr(0,found) << std::endl;
  std::cout << " file: " << str.substr(found+1) << std::endl;
}
//-------------------------------------------------------

#endif
