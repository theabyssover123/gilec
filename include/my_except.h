#ifndef __MY_EXCEPT_H
#define __MY_EXCEPT_H

#include "mystring.h"

//����� ���������� ����� �������� ��������� � �����������
// ������ ���������
// ���� � ��� ��� ���� ������� 
// void THROW_FSTR(int errc, LPCTSTR format, ...) /**/ 
// � ��� ������� vprintf (?)
// �� ���������� �� ����� ��������������� (? �� ���� ������)

//------------------------------------------------------------------------------------------
inline
void THROW_STR(int errc, LPCTSTR format) /**/
{
      throw  my_logic_error(errc, String(format));
}

//------------------------------------------------------------------------------------------
template <typename T1>
inline
void THROW_FSTR(int errc, LPCTSTR format, T1 arg1) /**/
{
      String s;
      sprintf(s, format, arg1);

      throw  my_logic_error(errc, s);
}
//------------------------------------------------------------------------------------------
template <typename T1, typename T2>
inline
void THROW_FSTR(int errc, LPCTSTR format, T1 arg1, T2 arg2) /**/
{
      String s;
      sprintf(s, format, arg1, arg2);

      throw  my_logic_error(errc, s);
}
//------------------------------------------------------------------------------------------
template <typename T1, typename T2, typename T3>
inline
void THROW_FSTR(int errc, LPCTSTR format, T1 arg1, T2 arg2, T3 arg3) /**/
{
      String s;
      sprintf(s, format, arg1, arg2, arg3);

      throw  my_logic_error(errc, s);
}

//------------------------------------------------------------------------------------------
#ifdef __INDESIGN
class mTHROW
{
  private:
    int _errc;
  public:
    mTHROW(int errc = -1)
	  : _errc(errc)
	{}
  mTHROW&  operator<< (const String &sout)
  {
      throw  my_logic_error(_errc, sout);
	  return *this;
  }
};
#endif

#endif
