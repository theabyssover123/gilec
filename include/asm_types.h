#ifndef __ASM_TYPES_H
#define __ASM_TYPES_H

#include <asm/types.h>

#ifdef _ASM_GENERIC_INT_LL64_H //not def u8
    typedef __s8     s8;
    typedef __u8     u8;
                     
    typedef __s16    s16;
    typedef __u16    u16;
                     
    typedef __s32    s32;
    typedef __u32    u32;
    
    typedef __s64    s64;
    typedef __u64    u64;

#endif

#endif
