#ifndef __COMMAND_PATTERN_H
#define __COMMAND_PATTERN_H


//--------------------------------------------------------------------
//����� ������� ������ (�����������)
//--------------------------------------------------------------------

class TCommand {
  public:
  private: //only for local
  public:  // for all
    virtual ~TCommand(){}
    virtual int execute() = 0;
  protected: //only for derived & local
    TCommand(){}
};



#endif
