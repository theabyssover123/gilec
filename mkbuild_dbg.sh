#!/bin/bash
#top=`cd .. && pwd`
top=`cd .. && pwd`
tc=`cd $top/ext-toolchain && pwd`
#tc=/media/Elems4/DartImx6/mypjsip/buildroot/output/host/usr
export PATH=$tc/bin:$PATH
export CROSS_COMPILE=arm-linux-gnueabihf-

STAGING=$top/staging
TARGET=$top/targetfs
PREFIX="/usr"
#nn rm -r $PREFIX/bin/pj
export PKG_CONFIG_PATH=$STAGING/usr/lib/pkgconfig:$STAGING/usr/local/lib/pkgconfig

${CROSS_COMPILE}gcc -v

check_err()
{
    if [ $? -ne 0 ]; then
       exit 1;
    fi
}

#cd $top/src

rm -rf ./build
mkdir ./build
cd build

cmake --version
cmake -DCMAKE_TOOLCHAIN_FILE=$top/toolchain_imx.cmake -DCMAKE_BUILD_TYPE=DEBUG -DCMAKE_INSTALL_PREFIX=/usr/local ..
check_err
make VERBOSE=1
check_err
sudo make DESTDIR=$TARGET install
check_err
