#include <iostream>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <ina219.h>
#include <TCA6416.h>
#include <TCA6408.h>


const char *SDI_CONFIG_ARG = "-s";
const char *MIPI_CONFIG_ARG = "-m";
const char *MIPI_PAL_CONFIG_ARG = "-p";
const char *PAL_SDI_CONFIG = "-x";
const char *HELP_ARG = "-h";
const char *INA_GET = "-i";

int main(int argc, char **argv) {
  
    int i;
    uint8_t ina = 0;
    uint8_t config = MODE_SDI;  
    if(argc == 1) {
	  printf("Command line has no additional arguments\n");
    }
    else
    {
      if(argc > 2){
	printf("Too much arguments, only 1 possible\n");
	return -1;
      }
      if(!strcmp(argv[1], HELP_ARG))
      {
	printf("Arg %s for help\n", SDI_CONFIG_ARG);
	printf("Arg %s for MIPI config\n", MIPI_CONFIG_ARG);
	printf("Arg %s for MIPI PAL config\n", MIPI_PAL_CONFIG_ARG);
	printf("Arg %s for SDI config\n", SDI_CONFIG_ARG);
	printf("Arg %s for INA 219 data\n", INA_GET);
	printf("Arg %s for PAL_SDI config\n", PAL_SDI_CONFIG);
	return 0;
      }
      else if(!strcmp(argv[1], SDI_CONFIG_ARG)){
	config = MODE_SDI; 
	printf("Configuring SDI MODE\n");
      }
      else if(!strcmp(argv[1], PAL_SDI_CONFIG)){
	config = MODE_SDI_PAL; 
	printf("Configuring SDI+PAL MODE\n");
      }
      else if(!strcmp(argv[1], MIPI_CONFIG_ARG)){
	config = MODE_MIPI;  
	printf("Configuring MIPI MODE\n");
      }
      else if(!strcmp(argv[1], MIPI_PAL_CONFIG_ARG)){
	config = MODE_MIPI_PAL;  
	printf("Configuring MIPI PAL MODE\n");
      }
      else if(!strcmp(argv[1], INA_GET)){
	ina = 1;
	printf("Get Ina data\n");
      }
      else{
	printf("Wrong argument\n");
      }
    }
    
    if(ina)
    {
      uint8_t dev = 0;
      for(dev = 0; dev < INA219_MAX_DEV; dev++){
	INA219_init(dev);
      }    
      for(dev = 0; dev < INA219_MAX_DEV; dev++){
	INA219_getVoltage(dev);
      } 
      return 0;
    }
    
    TCA6416_init(TCA6416_SD); 
    TCA6416_init(TCA6416_PLIS);   
    TCA6416_init(TCA6416_POWER);   
    TCA6416_configVideoMode(config);
    TCA6408_init();
       
    return 0;
}
