#ifndef __INA219_H
#define __INA219_H

#include <stdint.h>

#define INA219_ADDR_BASE 0x40
#define INA_MAIN1 0
#define INA_MAIN2 1
#define INA_SD 2
#define INA_ETH 3 
#define INA_VIDEO1 4 
#define INA_VIDEO2 5
#define INA_PW_CAM 6

#define INA219_MAX_DEV 7 

#define INA219_CURRENT_REG 	0x01
#define INA219_VOLTAGE_REG	0x02
#define INA219_CONFIG_REG 	0x00
#define INA219_CALIBRATION_REG 	0x00


#define INA219_CONFIG__RESET		(1 << 15)	

#define INA219_CONFIG__BRNG			(1 << 13)	//Bus Voltage Range
#define INA219_CONFIG__BRNG_16V		(0 << 13)	//16 V FSR
#define INA219_CONFIG__BRNG_32V		(1 << 13)	//32 V FSR

#define INA219_CONFIG__PG			(3 << 11)	//PGA (Shunt Votage Only)
#define INA219_CONFIG__PG_40mV		(0 << 11)	// +/- 40 mV Range - Gain = 1
#define INA219_CONFIG__PG_80mV		(1 << 11)	// +/- 80 mV Range - Gain = +2
#define INA219_CONFIG__PG_160mV		(2 << 11)	// +/- 160 mV Range - Gain = +4
#define INA219_CONGIG__PG_320mV		(3 << 11)	// +/- 320 mV Range - Gain = +8

#define INA219_CONGIG__BADC			(0x0F << 7)	//Êîëè÷åñòâî âûáîðîê äëÿ óñðåäíåíèÿ Bus Voltage Register
#define INA219_CONFIG__BADC_SHIFT	7 //Ñäâèã ïî ðåãèñòðó äëÿ ïîëó÷åíèÿ ñåìïëîâ
#define INA219_CONFIG__BADC_128_SAMPLE		(0x780)
#define INA219_CONFIG__BADC_2_SAMPLE		(0x480)
#define INA219_CONFIG__SADC				(0x0F << 3) //Êîëè÷åñòâî âûáîðîê äëÿ óñðåäíåíèÿ òîêà
#define INA219_CONFIG__SADC_9BIT 		(0x00 << 3) //9 áèò - 84 ìêÑ
#define INA219_CONFIG__SADC_10BIT 		(0x01 << 3) //10 áèò - 148 ìêÑ
#define INA219_CONFIG__SADC_11BIT		(0x02 << 3) //11 áèò - 276 ìêÑ
#define INA219_CONFIG__SADC_12BIT		(0x03 << 3) //12 áèò - 532 èêÑ
#define INA219_CONFIG__SADC_1SAMPLE		(0x08 << 3) //1 ñåìïë - 532 ìêÑ
#define INA219_CONFIG__SADC_2SAMPLE		(0x09 << 3) //2 ñåìïëà - 1.06 ìÑ
#define INA219_CONFIG__SADC_4SAMPLE 	(0x0A << 3) //4 ñåìïëà - 2.13 ìÑ
#define INA219_CONFIG__SADC_8SAMPLE		(0x0B << 3) //8 ñåìïëîâ - 4.26 ìÑ
#define INA219_CONFIG__SADC_16SAMPLE	(0x0C << 3) //16 ñåìïëîâ - 8.15 ìÑ
#define INA219_CONFIG__SADC_32SAMPLE	(0x0D << 3) //32 ñåïëà - 17.02 ìÑ
#define INA219_CONFIG__SADC_64SAMPLE	(0x0E << 3) //64 ñåìïëà - 43.05 mC
#define INA219_CONFIG__SADC_128SAMPLE	(0x0F << 3) //128 ñåìïëîâ - 68.10 ìÑ

#define INA219_CONFIG__MODE							(0x07 << 0) //ðåæèì ðàáîòû
#define INA219_CONFIG__MODE_POWERDOWN				(0x00 << 0)
#define INA219_CONFIG__MODE_SHUNT_TRIGGERED			(0x01 << 0)
#define INA219_CONFIG__MODE_BUS_TRIGGERED			(0x02 << 0)
#define INA219_CONFIG__MODE_SHUNT_AND_BUS_TRIGGERED (0x03 << 0)
#define INA219_CONFIG__MODE_ADC_OFF					(0x04 << 0)
#define INA219_CONFIG__MODE_SHUNT_CONTINUE			(0x05 << 0)
#define INA219_CONFIG__MODE_BUS_CONTINUE			(0x06 << 0)
#define INA219_CONFIG__MODE_SHUNT_AND_BUS_CONTINUE	(0x07 << 0)
#define INA219_CONFIG__BUS_DATA_SHIFT	3 //Ñäâèã äëÿ ïîëó÷åíèÿ äàííûõ


int INA219_getVoltage(uint8_t devNum);
int INA219_init(uint8_t devNum);

#endif 