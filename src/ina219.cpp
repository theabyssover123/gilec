#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <ina219.h>

int INA219_setRegvalue(uint8_t devAdress, uint8_t reg, uint16_t data)
{
    const char * devName = "/dev/i2c-2";
        
    int dev = open(devName, O_RDWR);
    if (dev == -1)
    {
	printf("Dev not opened Ina 219 number %d\r\n", devAdress);        
	close(dev);
        return -1;
    } 
    // Specify the address of the slave device.
    if (ioctl(dev, I2C_SLAVE, INA219_ADDR_BASE + devAdress) < 0)
    {
	printf("Failed bus Access Ina 219 number %d\r\n", devAdress);
        perror("Failed to acquire bus access and/or talk to slave TCA6416");
	close(dev);
        return -1;
    }      
   
    uint8_t writeBuf[3];
    writeBuf[0] = reg & 0x7F;
    writeBuf[1] = data >> 8; 
    writeBuf[2] = data & 0xff;
    
    
    if (write(dev,&writeBuf[0],3) != 3) {
      printf("Failed bus Access Ina 219 number %d\r\n", devAdress);	    
      close(dev);
      return -1;
    }	
    
    close(dev);
    
    return 0;
}

int INA219_getRegvalue(uint8_t devAdress, uint8_t reg, uint16_t * data)
{	
	int status = 0; 
	const char * devName = "/dev/i2c-2";
	int dev = open(devName, O_RDWR);
	uint8_t readData[2];
	
	if (dev == -1)
	{
	    printf("Dev not opened Ina 219 number %d\r\n", devAdress);	    
	    close(dev);
	    return -1;
	} 
	// Specify the address of the slave device.
	if (ioctl(dev, I2C_SLAVE, INA219_ADDR_BASE + devAdress) < 0)
	{
	    printf("Failed bus Access Ina 219 number %d\r\n", devAdress);	    
	    close(dev);
	    return -1;
	}      
	
	 if (write(dev,&reg, 1) != 1) {
	  printf("Failed bus Access Ina 219 number %d\r\n", devAdress);	    
	  close(dev);
	  return -1;
	}
	
	if (read(dev,&readData[0],2) != 2) {
	  perror("write failed");
	  close(dev);
	  return -1;
	}
    
	*data = ((readData[0] << 8) | (readData[1]));	
	
	close(dev);
	
	return 0;
}

int INA219_getVoltage(uint8_t devNum)
{
    int status = 0;
    
    uint16_t voltage = 0;
    
    status = INA219_getRegvalue(devNum, INA219_VOLTAGE_REG, &voltage);
    
    if(!status)
    {
      voltage = (voltage >> INA219_CONFIG__BUS_DATA_SHIFT) * 4;
      printf("Voltage on Ina with adress: %d is %d\r\n", (INA219_VOLTAGE_REG + devNum) , voltage);
      return 0;
    }    
    else
      return -1;
}

int INA219_init(uint8_t devNum)
{
  
    int status = 0;   
    
    uint16_t config = (0x0000 | INA219_CONFIG__BRNG_16V |	
			INA219_CONFIG__PG_40mV |		
			INA219_CONFIG__BADC_128_SAMPLE |	
			INA219_CONFIG__SADC_12BIT | 	
			INA219_CONFIG__MODE_BUS_CONTINUE | INA219_CONFIG__RESET) ;			
			
  
    status = INA219_setRegvalue(devNum, INA219_CONFIG_REG, config);
   
    return status;
}

