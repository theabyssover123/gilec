


#define TCA6416_PLIS 1
#define TCA6416_POWER 2
#define TCA6416_SD 3

#define TCA6416_PLIS_ADDRESS 0x21
#define TCA6416_POWER_ADDRESS 0x20
#define TCA6416_SD_ADDRESS 0x20


#define INPUT_REG_0 0x00
#define INPUT_REG_1 0x01
#define OUTPUT_REG_0 0x02
#define OUTPUT_REG_1 0x03
#define INVERS_REG_0 0x04
#define INVERS_REG_1 0x05
#define CONFIG_REG_0 0x06
#define CONFIG_REG_1 0x07

#define PIN_INPUT 1	modprobe
#define PIN_OUTPUT 0


////TCA_SD
#define PORT_0_OUTPUT_PIN0 (1 << 0)
#define PORT_0_OUTPUT_PIN1 (1 << 1)
#define PORT_0_OUTPUT_PIN2 (1 << 2)
#define PORT_0_OUTPUT_PIN3 (1 << 3)
#define PORT_0_OUTPUT_PIN4 (1 << 4)
#define PORT_0_INPUT_SD_PRESENT_MMC2 (1 << 5)
#define PORT_0_INPUT_SD1_TEST (1 << 6)
#define PORT_0_OUTPUT_SD1_ENA (1 << 7)

#define PORT_1_OUTPUT_SD2_ENA (1 << 0)
#define PORT_1_OUTPUT_IND_ENA (1 << 1)
#define PORT_1_OUTPUT_LED_RED_1 (1 << 2)
#define PORT_1_OUTPUT_LED_GREEN_1 (1 << 3)
#define PORT_1_OUTPUT_LED_BLUE_1 (1 << 4)
#define PORT_1_OUTPUT_LED_RED_2 (1 << 5)
#define PORT_1_OUTPUT_LED_GREEN_2 (1 << 6)
#define PORT_1_OUTPUT_LED_BLUE_2 (1 << 7)

//TCA_PLIS

#define PORT_0_OUTPUT_XCDI0 (1 << 0)
#define PORT_0_OUTPUT_XCDI1 (1 << 1)
#define PORT_0_OUTPUT_XCDI2 (1 << 2)
#define PORT_0_OUTPUT_XCDI3 (1 << 3)
#define PORT_0_OUTPUT_XCA0 (1 << 4)
#define PORT_0_OUTPUT_XCA1 (1 << 5)
#define PORT_0_OUTPUT_XC_DI_LOCK (1 << 6)
#define PORT_0_OUTPUT_TMR_IRQ_EN (1 << 7)

#define PORT_1_OUTPUT_PLIS_LED_RED_1 (1 << 0)
#define PORT_1_OUTPUT_PLIS_LED_GREEN_1 (1 << 1)
#define PORT_1_OUTPUT_PLIS_LED_BLUE_1 (1 << 2)
#define PORT_1_OUTPUT_PLIS_LED_RED_2 (1 << 3)
#define PORT_1_OUTPUT_PLIS_LED_GREEN_2 (1 << 4)
#define PORT_1_OUTPUT_PLIS_LED_BLUE_2 (1 << 5)
#define PORT_1_OUTPUT_PLIS_RESERV1 (1 << 6)
#define PORT_1_OUTPUT_PLIS_RESERV2 (1 << 7)

//TCA_POWER

#define MODE_SDI 1
#define MODE_MIPI 2
#define MODE_MIPI_PAL 3
#define MODE_SDI_PAL 4

#define PORT_0_OUTPUT_MAIN1_ENA (1 << 0)
#define PORT_0_OUTPUT_MAIN2_ENA (1 << 1)
#define PORT_0_OUTPUT_USB_ENA (1 << 2)
#define PORT_0_OUTPUT_ETH_ENA (1 << 3)
#define PORT_0_OUTPUT_ETH_BX_ENA (1 << 4)
#define PORT_0_OUTPUT_PW_CAM_ENA (1 << 5)
#define PORT_0_OUTPUT_VANLG_ENA (1 << 6)
#define PORT_0_OUTPUT_RST_ANLG (1 << 7)

#define PORT_1_OUTPUT_PW_VIDEO1_ENA (1 << 0)
#define PORT_1_OUTPUT_PW_VIDEO2_ENA (1 << 1)
#define PORT_1_OUTPUT_RESET_VIDEO_1 (1 << 2)
#define PORT_1_OUTPUT_RESET_VIDEO_2 (1 << 3)
#define PORT_1_OUTPUT_PW_CSIX_ENA (1 << 4)
#define PORT_1_OUTPUT_CSIX_ENA (1 << 5)
#define PORT_1_OUTPUT_CSIX_AB (1 << 6)
#define PORT_1_OUTPUT_RESERV (1 << 7)

int TCA6416_configVideoMode(uint8_t mode);
int TCA6416_init(uint8_t devNum);