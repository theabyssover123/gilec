#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <TCA6408.h>


int TCA6408_init()
{
    uint8_t i;
    uint8_t buf[10];
    const char * devName = "/dev/i2c-0";
    
    printf("Config TCA6408\r\n");
    // Open up the I2C bus
    int dev = open(devName, O_RDWR);
    if (dev == -1)
    {
	printf("Dev not opened TCA6408\r\n");
        perror(devName);
	close(dev);
        return -1;
    } 
    // Specify the address of the slave device.
    if (ioctl(dev, I2C_SLAVE, TCA6408_ADDRESS) < 0)
    {
	printf("Failed bus Access TCA6408\r\n");
        perror("Failed to acquire bus access and/or talk to slave TCA6408");
	close(dev);
        return -1;
    }
    
    buf[0] = CONFIG_REG; 
    buf[1] = 0x00; 
    if (write(dev,buf, 2) != 2) {
        perror("write failed TCA6408");
        close(dev);
        return -1;
    }
    
    buf[2] = OUTPUT_REG; 
    buf[3] = PORT_0_OUTPUT_LIN; 
    if (write(dev,&buf[2], 2) != 2) {
        perror("write failed output reg TCA6408");
        close(dev);
        return -1;
    }
    
    
    close(dev);   
    
    
    return 0;
}