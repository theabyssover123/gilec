



#define TCA6408_ADDRESS 0x21

#define INPUT_REG  0x00
#define OUTPUT_REG 0x01
#define INVERS_REG 0x02
#define CONFIG_REG 0x03


#define PORT_0_OUTPUT_MICAGA0 (1 << 0)
#define PORT_0_OUTPUT_MICGA1 (1 << 1)
#define PORT_0_OUTPUT_PWRMIC (1 << 2)
#define PORT_0_OUTPUT_HPF_SEL (1 << 3)
#define PORT_0_OUTPUT_LIN (1 << 4)
#define PORT_0_OUTPUT_RES1 (1 << 5)
#define PORT_0_OUTPUT_RES2 (1 << 6)
#define PORT_0_OUTPUT_RES3 (1 << 7)


int TCA6408_init();
