#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <TCA6416.h>



int TCA6416_init(uint8_t devNum)
{
    uint8_t i;
    uint8_t buf[10];
    const char * devName;
    uint8_t address = 0;
    switch(devNum)
    {
      case TCA6416_PLIS:
      {
	printf("Config TCA PLIS\r\n");
	address = TCA6416_PLIS_ADDRESS;
	devName = "/dev/i2c-2";	
	buf[0] = CONFIG_REG_0;
	buf[1] = 0x00;
	buf[2] = CONFIG_REG_1;
	buf[3] = 0x00;
	buf[4] = OUTPUT_REG_0;
	buf[5] = 0x00;
	buf[6] = OUTPUT_REG_1;
	buf[7] = PORT_1_OUTPUT_PLIS_LED_BLUE_1 | PORT_1_OUTPUT_PLIS_LED_BLUE_2;
	break;
      }
      case TCA6416_POWER:
      {
	printf("Config TCA POWER\r\n");
	address = TCA6416_POWER_ADDRESS;
	devName = "/dev/i2c-2"	;
	buf[0] = CONFIG_REG_0;
	buf[1] = 0x08;
	buf[2] = CONFIG_REG_1;
	buf[3] = 0x00;
	buf[4] = OUTPUT_REG_0;
	buf[5] = PORT_0_OUTPUT_MAIN1_ENA | PORT_0_OUTPUT_VANLG_ENA | PORT_0_OUTPUT_RST_ANLG;
	buf[6] = OUTPUT_REG_1;
	buf[7] = PORT_1_OUTPUT_PW_VIDEO1_ENA;
	break;
      }
      case TCA6416_SD:
      {
	printf("Config TCA SD\r\n");
	address = TCA6416_SD_ADDRESS;
	devName = "/dev/i2c-0"	;
	buf[0] = CONFIG_REG_0;
	buf[1] = 0x60;
	buf[2] = CONFIG_REG_1;
	buf[3] = 0x00;
	buf[4] = OUTPUT_REG_0;
	buf[5] = PORT_0_OUTPUT_SD1_ENA;	
	buf[6] = OUTPUT_REG_1;
	buf[7] = PORT_1_OUTPUT_IND_ENA | PORT_1_OUTPUT_LED_BLUE_1 | PORT_1_OUTPUT_LED_RED_2;
	break;
      }
      
    }
       
    // Open up the I2C bus
    int dev = open(devName, O_RDWR);
    if (dev == -1)
    {
	printf("Dev not opened TCA6416\r\n");
        perror(devName);
	close(dev);
        return -1;
    } 
    // Specify the address of the slave device.
    if (ioctl(dev, I2C_SLAVE, address) < 0)
    {
	printf("Failed bus Access TCA6416\r\n");
        perror("Failed to acquire bus access and/or talk to slave TCA6416");
	close(dev);
        return -1;
    }
 
    if (write(dev,&buf[0],2) != 2) {
        perror("write failed");
        close(dev);
        return -1;
    }

    if (write(dev,&buf[2],2) != 2) {
        perror("write failed");
        close(dev);
        return -1;
    }
          
    
    if (write(dev,&buf[4],2) != 2) {
        perror("write failed");
        close(dev);
        return -1;
    }
    
     if (write(dev,&buf[6],2) != 2) {
        perror("write failed");
        close(dev);
        return -1;
    }
    
    close(dev);   
   
    
    return 0;
}

int TCA6416_enableEthernet(uint8_t mode)
{
  if(mode){
    
  
  }
  
}
int TCA6416_configVideoMode(uint8_t mode)
{ 
    const char * devName = "/dev/i2c-2";
    uint8_t address = 0;
    uint8_t buf[10];
    printf("Configuring video!!!\r\n");
    
    int dev = open(devName, O_RDWR);
    if (dev == -1)
    {
	printf("Dev not opened TCA6416 power\r\n");
        perror(devName);
	close(dev);
        return -1;
    } 
    // Specify the address of the slave device.
    if (ioctl(dev, I2C_SLAVE, TCA6416_POWER_ADDRESS) < 0)
    {
	printf("Failed bus Access TCA6416 Power\r\n");
        perror("Failed to acquire bus access and/or talk to slave TCA6416");
	close(dev);
        return -1;
    }
    
    buf[0] = OUTPUT_REG_1;
    if (write(dev,&buf[0],1) != 1) {
        perror(" TCA6416 Power write failed");
        close(dev);
        return -1;
    }
       
    if (read(dev,&buf[1],1) != 1) {
        perror("TCA6416 Power read failed");
        close(dev);
        return -1;
    }+
    printf("Configuring Mode!!!\r\n");
    switch(mode)
    {
      case MODE_SDI:
      {
	printf("Writing SDI config\r\n");
	buf[0] = OUTPUT_REG_0;
	buf[1] = PORT_0_OUTPUT_MAIN1_ENA | PORT_0_OUTPUT_VANLG_ENA | PORT_0_OUTPUT_ETH_BX_ENA;// | PORT_0_OUTPUT_RST_ANLG;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}	
	buf[0] = OUTPUT_REG_1;
	buf[1] = PORT_1_OUTPUT_PW_VIDEO1_ENA;	
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}
	break;
      }
      case MODE_SDI_PAL:
      {
	printf("Writing SDI+PAL config\r\n");	
	buf[0] = OUTPUT_REG_0;
	buf[1] = PORT_0_OUTPUT_MAIN1_ENA | PORT_0_OUTPUT_VANLG_ENA | PORT_0_OUTPUT_ETH_BX_ENA | PORT_0_OUTPUT_MAIN2_ENA;// | PORT_0_OUTPUT_RST_ANLG;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}	
	buf[0] = OUTPUT_REG_1;
	buf[1] = PORT_1_OUTPUT_PW_VIDEO1_ENA | PORT_1_OUTPUT_PW_VIDEO2_ENA;	
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}	
	buf[1] |= PORT_1_OUTPUT_RESET_VIDEO_2;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}
	buf[1] &= ~PORT_1_OUTPUT_CSIX_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}
	buf[1] |= PORT_1_OUTPUT_PW_CSIX_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;	
	}
	break;
      }
      case MODE_MIPI:
      {
	printf("Writing Mipi config\r\n");
	buf[0] = OUTPUT_REG_0;
	buf[1] = PORT_0_OUTPUT_MAIN1_ENA | PORT_0_OUTPUT_MAIN2_ENA | PORT_0_OUTPUT_VANLG_ENA | PORT_0_OUTPUT_ETH_BX_ENA | PORT_0_OUTPUT_RST_ANLG; // открыл 
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");        
	  close(dev);
	  return -1;
	}	
	buf[0] = OUTPUT_REG_1;
	buf[1] = PORT_1_OUTPUT_PW_VIDEO2_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}
	buf[1] |= PORT_1_OUTPUT_CSIX_AB;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}	
	buf[1] &= ~PORT_1_OUTPUT_CSIX_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}
	buf[1] |= PORT_1_OUTPUT_PW_CSIX_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}	
	break;
      }
      case MODE_MIPI_PAL:
      {
	printf("Writing Mipi Pal config\r\n");
	buf[0] = OUTPUT_REG_0;
	buf[1] = PORT_0_OUTPUT_MAIN1_ENA | PORT_0_OUTPUT_MAIN2_ENA | PORT_0_OUTPUT_VANLG_ENA | PORT_0_OUTPUT_PW_CAM_ENA | PORT_0_OUTPUT_ETH_BX_ENA;// | PORT_0_OUTPUT_RST_ANLG;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");    
	  close(dev);
	  return -1;
	}	
	buf[0] = OUTPUT_REG_1;
	buf[1] = PORT_1_OUTPUT_PW_VIDEO2_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}	
	buf[1] |= PORT_1_OUTPUT_RESET_VIDEO_2;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}
	buf[1] &= ~PORT_1_OUTPUT_CSIX_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}
	buf[1] |= PORT_1_OUTPUT_PW_CSIX_ENA;
	if (write(dev,&buf[0],2) != 2) {
	  perror(" TCA6416 Power writing mipi cfg failed");
	  close(dev);
	  return -1;
	}	
	break;
      }
      default:
      {
	printf("Wrong Mode!!!\r\n");
	close(dev);
	return -1;
      }
    }
    return 0;  
}