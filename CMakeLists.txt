cmake_minimum_required(VERSION 2.8)
project( gilec_proj )

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH} )

#include( ${CMAKE_SOURCE_DIR}/cmake/pkg_check_modules.cmake )
#include( ${CMAKE_SOURCE_DIR}/cmake/axis_lib.cmake )

set(CMAKE_INSTALL_PREFIX /usr/)

find_package(PkgConfig REQUIRED)
pkg_search_module(AXISLIB REQUIRED axis-lib)
pkg_search_module(DISPATCHERLIB REQUIRED dispatcher-lib)
pkg_search_module(GIOUNIX REQUIRED gio-unix-2.0)

#find_package( OpenCV  2.4 REQUIRED )
find_package( GStreamer 0.10 REQUIRED )
find_package( GLIB REQUIRED )
#find_package( OpenSSL REQUIRED )

add_definitions(-std=c++11)
add_definitions(-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64)

set(myProjIncsDirs 
	./
	./src
	./src/include
	./src/flash
	./include
)

foreach (_dir ${myProjIncsDirs})
	file(GLOB SubSrcs "${_dir}/*.cpp" )
	list(APPEND myProjSrcs  ${SubSrcs} )
endforeach()

#===http://stackoverflow.com/questions/10076820/cmake-pkg-config-library-link-path
link_directories(${AXISLIB_LIBRARY_DIRS} ${DISPATCHERLIB_LIBRARY_DIRS})

add_executable( ${PROJECT_NAME} ${myProjSrcs} )

include_directories(
	${myProjIncsDirs}
    ${GLIB_INCLUDES}
    ${GSTREAMER_INCLUDE_DIRS}
	./include
	${AXISLIB_INCLUDE_DIRS}
	${DISPATCHERLIB_INCLUDE_DIRS}
  	${GIOUNIX_INCLUDE_DIRS}
)

message("========== ${PROJECT_NAME} \
    ${GLIB_INCLUDES} \
    ${GSTREAMER_INCLUDE_DIRS} \
	${GSTREAMER_APP_INCLUDE_DIRS} \
"
)

#target_link_libraries( ${PROJECT_NAME}  
#	${GSTREAMER_LIBRARIES}
#	${GLIB_LIBRARIES} 
#)

target_link_libraries( ${PROJECT_NAME}  
  ${GSTREAMER_APP_LIBRARIES}
  ${GSTREAMER_LIBRARIES}
  libgthread-2.0.so
  libgobject-2.0.so
  libglib-2.0.so
  libpthread.so
  librt.so
#  libgio-2.0.so
  ${GIOUNIX_LIBRARIES}
  ${AXISLIB_LIBRARIES}
  ${DISPATCHERLIB_LIBRARIES}
)


install (TARGETS ${PROJECT_NAME} DESTINATION bin)

